<?php

// class Home
class Home extends Controller {

    private $dt;
    private $df;
    public function __construct(){
        $this->dt = $this->loadmodel("barang"); //object
        $this->df = $this->loadmodel("daftarBarang");
        // echo "anda berada pada controller index \n";
    }
    public function index(){
        echo "anda memanggil action index \n";
    }
    public function home($data1, $data2){
        echo "anda memanggil action home dengan data 1 = $data1 dan data 2 = $data2 \n";
    }
    public function lihatdata(){
        // echo $this->dt->getData();
        $data = $this->dt->getDataOne();

        $this->loadview('templates/header', ['title'=>'Detail Barang']);
        $this->loadview('home/detailbarang',$data);
        $this->loadview('templates/footer');

    }
    public function listbarang(){
        $data = $this->df->getDataAll();

        $this->loadview('templates/header', ['title'=>'List Barang']);
        $this->loadview('home/listbarang',$data);
        $this->loadview('templates/footer');

        // foreach ($this->df->getDataAll() as $item) {  
        // echo $item['id']. " " . $item["nama"] ." ". $item['qty'];
        // echo "</br />";
        // }
    }
    public function insertBarang(){
        $this->loadview('Templates/header', ['title'=>'Insert Barang']);
        $this->loadview('home/form');
        $this->loadview('Templates/footer');
    }

}

?>